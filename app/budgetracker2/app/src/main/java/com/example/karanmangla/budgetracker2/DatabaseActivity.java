package com.example.karanmangla.budgetracker2;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class DatabaseActivity extends Activity implements OnClickListener {

    EditText et1,et2,et3;
    Button bt1,bt2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        et1 = (EditText)findViewById(R.id.editText1);
        et2 = (EditText)findViewById(R.id.editText2);
        //et3 = (EditText)findViewById(R.id.editText3);
        bt1 = (Button) findViewById(R.id.button1);
        bt2 = (Button) findViewById(R.id.button2);
        bt1.setOnClickListener(this);
        bt2.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.my, menu);
        return true;
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch(v.getId())
        {
            //save
            case R.id.button1:
                //Toast.makeText(this, "b1 work", Toast.LENGTH_SHORT).show();
                String name = et1.getText().toString();
                String tech = et2.getText().toString();
                //String amt = et3.getText().toString();
                DBHelper dbh = new DBHelper(this);
                dbh.open();
                dbh.write(name, tech);
                dbh.close();
                Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();
                break;
            //view
            case R.id.button2:
                Toast.makeText(this, "b2 work", Toast.LENGTH_SHORT).show();
                DBHelper dbh1 = new DBHelper(this);
                dbh1.open();
                String result = dbh1.getData();
                dbh1.close();

                Intent showActivity = new Intent(this, MyActivity.class);
                showActivity.putExtra("Result", result);
                startActivity(showActivity);
                break;
        }
    }

}
