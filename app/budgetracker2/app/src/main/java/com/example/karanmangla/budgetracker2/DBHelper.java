package com.example.karanmangla.budgetracker2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

public class DBHelper {

    public static final String DB_NAME = "TEST_DBNAME";
    public static final int DB_VERSION = 5;
    public static final String DB_TABLE = "TEST_DBTABLE";
    public static final String C_Name = "Name";
    public static final String C_Tech = "Technology";
    //public static final String C_Amt = "Amount";
    public static final String CREATE_QUERY = "CREATE TABLE " + DB_TABLE + "(" + C_Name + " text not null, "
            + C_Tech + " text not null);";
    public static final String DROP_QUERY = "DROP TABLE " + DB_TABLE + ";";

    Context ourContext;
    DBHelperInternal dbhi;
    SQLiteDatabase database;

    public DBHelper(Context c) {
        ourContext = c;
    }

    private class DBHelperInternal extends SQLiteOpenHelper{

        public DBHelperInternal(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_QUERY);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DROP_QUERY);
            onCreate(db);
        }

    }

    public DBHelper open() {
        dbhi = new DBHelperInternal(ourContext);
        database = dbhi.getWritableDatabase();
        return this;
    }

    public void write(String name, String tech) {
        //The values we enter in database is known as content values.
        //These are handled by ContentValues class and its put method.
        ContentValues cv = new ContentValues();
        cv.put(C_Name, name);
        cv.put(C_Tech, tech);
        //cv.put(C_Amt,amt);
        database.insert(DB_TABLE, null, cv);
        //Toast.makeText(this, "Saved", Toast.LENGTH_SHORT).show();

    }

    public void close() {
        database.close();
    }

    public String getData() {
        String result = "";
        String[] columns = {C_Name, C_Tech };
        //	Cursor c = database.rawQuery("SELECT * FROM " + DB_TABLE, null);
        Cursor c = database.query(DB_TABLE, columns, null, null, null, null,null);
        int iName = c.getColumnIndex(C_Name);
        int iTech = c.getColumnIndex(C_Tech);
        //int iAmt = c.getColumnIndex(C_Amt);
        //String[][] arr = new String[1000][2];
        int i = 0;
        for(c.moveToFirst();!c.isAfterLast();c.moveToNext())
        {
            //arr[i][0] = c.getString(iName) ;
            //arr[i][1] = c.getString(iTech) ;
            result = result  + c.getString(iName) + "\n" + " -> " + c.getString(iTech)  + "\n";
            i++;
        }
        return result;


    }

}
